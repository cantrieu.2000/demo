import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:untitled/res/colors.dart';
import 'package:get/get.dart';
import 'package:untitled/ui/register/RegisterController.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Register Screen"),
        centerTitle: true,
      ),
      body: BodyWidget(),
    );
  }
}

class BodyWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BodyWidget();
}

class _BodyWidget extends State<BodyWidget> {
  final RegisterController _registerController = Get.put(RegisterController());
  String dropdownvalue = "Apple";

  List<String> items = [
    'Apple',
    'Banana',
    'Grapes',
    'Orange',
    'watermelon',
    'Pineapple'
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterController>(
        init: _registerController,
        builder: (registerController) {
          return Scaffold(
              body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  _buildTitle("Bản thân"),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        _buildTextInputHoTen(registerController, "Họ tên"),
                        _buildTextInputGioiTinh(
                            registerController, "Giới tính"),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        _buildTextInputNgheNghiep(
                            registerController, "nghề nghiệp"),
                        _buildTextInputQuocTich(
                            registerController, "Quốc tịch"),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  _buildTitle("Địa chỉ đăng kí theo hộ khẩu"),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        _buildTextInputSoNha(registerController, "Số nhà"),
                        _buildTextInputXaPhuong(
                            registerController, "Xã,phường"),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        _buildTextInputQuanHuyen(
                            registerController, "Quận,huyện"),
                        _buildTextInputTinh(registerController, "Tỉnh"),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          alignment: Alignment.centerLeft,
                          child: Text("Date"),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: new BoxDecoration(
                            border: Border.all(
                              width: 1.0,
                            ),
                          ),
                          child: FlatButton(
                            child: Obx(
                                () => Text(registerController.ngaySinh.value)),
                            onPressed: () {
                              dateTime(registerController);
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 15),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Nhap them thong tin",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                  ),
                  Divider(height: 5),
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        _buildTextInputCode(registerController, "Code"),
                        SizedBox(
                          width: 15,
                        ),
                        Container(
                          alignment: Alignment.center,
                          decoration: new BoxDecoration(
                            border: Border.all(
                              width: 1.0,
                            ),
                          ),
                          child: FlatButton(
                            child: Text("ĐỒng ý"),
                            onPressed: () {
                              dateTime(registerController);
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  _buildTextInputCmt(registerController, "Căn cước công dân"),
                  _buildTextInputTrinhDoVH(
                      registerController, "Trình độ văn hóa"),
                  _buildTextInputSoTruong(registerController, "Sở trường"),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Nhap them thong tin",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                  ),
                  Divider(height: 5),
                  _buildTextInputPhone(registerController, "Số điện thoại"),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Nhap them thong tin",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Thông tin đăng nhập",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  _buildTextInputEmail(registerController, "Email"),
                  SizedBox(
                    height: 10,
                  ),
                  _buildTitle("Ảnh đại diện"),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 15),
                    child: Stack(
                      children: [
                        ClipRRect(
                          child: Image.network(
                            'https://pbs.twimg.com/profile_images/1374979417915547648/vKspl9Et.jpg',
                            height: 150,
                            width: 150,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(100),
                        ),
                        Positioned(
                          bottom: 10,
                          right: 0,
                          child: Material(
                              borderRadius: BorderRadius.circular(25.0),
                              child: Container(
                                padding: EdgeInsets.all(5),
                                decoration: new BoxDecoration(
                                  borderRadius: BorderRadius.circular(25.0),
                                  border: Border.all(
                                    width: 2.0,
                                  ),
                                ),
                                child: Icon(
                                  Icons.camera_alt,
                                  size: 28,
                                ),
                              )),
                        ),
                      ],
                    ),
                  ),
                  _buildTextInputUserName(registerController, "Tên đăng nhập"),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    width: MediaQuery.of(context).size.width,
                    child: Text(""),
                  ),
                  Container(
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: dropdownvalue,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      elevation: 1,
                      onChanged: (newValue) {
                        setState(() {
                          dropdownvalue = newValue!;
                        });
                      },
                      items:
                          items.map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  Container(
                    width: 100,
                    margin: EdgeInsets.symmetric(vertical: 10),
                    alignment: Alignment.center,
                    decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(14.0),
                      color: Colors.amber,
                    ),
                    child: FlatButton(
                      child: Text("ĐỒng ý"),
                      onPressed: () {
                        registerController.validator();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ));
        });
  }

  dateTime(RegisterController homeController) {
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(1950, 1, 1),
        maxTime: DateTime.now(), onChanged: (date) {
      homeController.ngaySinh.value =
          '${date.day.toString()} - ${date.month.toString()} - ${date.year.toString()} ';
    }, onConfirm: (date) {
      print('confirm $date');
    }, currentTime: DateTime.now(), locale: LocaleType.vi);
  }

  Widget _buildTitle(String title) {
    return Container(
        padding: EdgeInsets.all(16),
        color: bgTitle,
        child: Row(
          children: [
            Container(
              child: Text(
                title,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
            ),
            SizedBox(
              width: 10,
            ),
          ],
        ));
  }

  Widget _buildTextInputHoTen(RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: 175,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errHoTen.value == ""
                    ? null
                    : homeController.errHoTen.value,
              ),
              onChanged: (value) {
                homeController.hoTen.value = value;
              },
              validator: null,
            ),
          ],
        ));
  }

  Widget _buildTextInputGioiTinh(
      RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: 175,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errGioiTinh.value == ""
                    ? null
                    : homeController.errGioiTinh.value,
              ),
              onChanged: (value) {
                homeController.gioiTinh.value = value;
              },
              validator: null,
            )
          ],
        ));
  }

  Widget _buildTextInputNgheNghiep(
      RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: 175,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errNgheNghiep.value == ""
                    ? null
                    : homeController.errNgheNghiep.value,
              ),
              onChanged: (value) {
                homeController.ngheNghiep.value = value;
              },
              validator: null,
            )
          ],
        ));
  }

  Widget _buildTextInputQuocTich(
      RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: 175,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errQuocTich.value == ""
                    ? null
                    : homeController.errQuocTich.value,
              ),
              onChanged: (value) {
                homeController.quocTich.value = value;
              },
              validator: null,
            ),
          ],
        ));
  }

  Widget _buildTextInputSoNha(RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: 175,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errsoNha.value == ""
                    ? null
                    : homeController.errsoNha.value,
              ),
              onChanged: (value) {
                homeController.soNha.value = value;
              },
              validator: null,
            ),
          ],
        ));
  }

  Widget _buildTextInputXaPhuong(
      RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: 175,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errxaPhuong.value == ""
                    ? null
                    : homeController.errxaPhuong.value,
              ),
              onChanged: (value) {
                homeController.xaPhuong.value = value;
              },
              validator: null,
            )
          ],
        ));
  }

  Widget _buildTextInputQuanHuyen(
      RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: 175,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errquanHuyen.value == ""
                    ? null
                    : homeController.errquanHuyen.value,
              ),
              onChanged: (value) {
                homeController.quanHuyen.value = value;
              },
              validator: null,
            )
          ],
        ));
  }

  Widget _buildTextInputTinh(RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: 175,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errTinh.value == ""
                    ? null
                    : homeController.errTinh.value,
              ),
              onChanged: (value) {
                homeController.tinh.value = value;
              },
              validator: null,
            ),
          ],
        ));
  }

  Widget _buildTextInputCmt(RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errCmt.value == ""
                    ? null
                    : homeController.errCmt.value,
              ),
              onChanged: (value) {
                homeController.cmt.value = value;
              },
              validator: null,
            )
          ],
        ));
  }

  Widget _buildTextInputTrinhDoVH(
      RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errtrinhDoVH.value == ""
                    ? null
                    : homeController.errtrinhDoVH.value,
              ),
              onChanged: (value) {
                homeController.trinhDoVH.value = value;
              },
              validator: null,
            )
          ],
        ));
  }

  Widget _buildTextInputSoTruong(
      RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errSoTruong.value == ""
                    ? null
                    : homeController.errSoTruong.value,
              ),
              onChanged: (value) {
                homeController.soTruong.value = value;
              },
              validator: null,
            )
          ],
        ));
  }

  Widget _buildTextInputEmail(RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errEmail.value == ""
                    ? null
                    : homeController.errEmail.value,
              ),
              onChanged: (value) {
                homeController.email.value = value;
              },
              validator: null,
            )
          ],
        ));
  }

  Widget _buildTextInputCode(RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: 175,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errcode.value == ""
                    ? null
                    : homeController.errcode.value,
              ),
              onChanged: (value) {
                homeController.code.value = value;
              },
              validator: null,
            )
          ],
        ));
  }

  Widget _buildTextInputPhone(RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errphone.value == ""
                    ? null
                    : homeController.errphone.value,
              ),
              onChanged: (value) {
                homeController.phone.value = value;
              },
              validator: null,
            )
          ],
        ));
  }

  Widget _buildTextInputUserName(
      RegisterController homeController, String title) {
    return Container(
        alignment: Alignment.centerLeft,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                hintText: '',
                labelText: title,
                labelStyle: TextStyle(color: primaryColor),
                errorText: homeController.errUserName.value == ""
                    ? null
                    : homeController.errUserName.value,
              ),
              onChanged: (value) {
                homeController.userName.value = value;
              },
              validator: null,
            )
          ],
        ));
  }
}
