import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:untitled/models/Product.dart';
import 'package:untitled/ui/home/tabs/tabHome/TabHomeController.dart';

Widget createSearchView(
    TabHomeController tabHomeController, BuildContext context) {
  return new Container(
    margin: EdgeInsets.all(8),
    width: MediaQuery.of(context).size.width,
    padding: EdgeInsets.symmetric(horizontal: 16),
    decoration: BoxDecoration(
        color: Colors.grey[300],
        borderRadius: BorderRadius.all(Radius.circular(25))),
    child: TextFormField(
      decoration: new InputDecoration(
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          icon: Icon(Icons.search),
          hintText: "Search"),
      textAlign: TextAlign.start,
      onChanged: (value) {
        tabHomeController.query.value = value;
        tabHomeController.search();
      },
    ),
  );
}

Widget createSearchListView(TabHomeController tabHomeController) {
  return Container(
    height: 150,
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: tabHomeController.performSearch().length,
      itemBuilder: (context, index) {
        return Card(
          margin: EdgeInsets.all(8),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Image.network(
                  '${tabHomeController.performSearch()[index].image}',
                  fit: BoxFit.cover,
                  height: 150,
                  width: 250,
                ),
                Positioned.fill(
                  child: Center(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: 0.0,
                        sigmaY: 0.0,
                      ),
                      child: Container(
                        color: Colors.black.withOpacity(0.4),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 20,
                  child: Text(
                    '${tabHomeController.performSearch()[index].title}',
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.white70,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    ),
  );
}
