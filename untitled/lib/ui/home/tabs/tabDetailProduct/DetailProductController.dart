import 'package:get/get.dart';
import 'package:untitled/contants/Contants.dart';
import 'package:untitled/models/Product.dart';
import 'package:untitled/services/FirebaseApi.dart';

class DetailProductController extends GetxController {
  var count = 1.obs;

  void addProduct() {
    if (count.value < 1) {
      return;
    }
    count.value = count.value + 1;
    update();
  }

  void removeProduct() {
    if (count.value <= 1) {
      return;
    }
    count.value = count.value - 1;
    update();
  }

  Future addProductFirebase(Product product, int quantity) async {
    FirebaseApi.refCartProduct
        .doc(Contants.account!.user?.uid)
        .collection(Contants.DETAIL_CART)
        .add({
      'id': product.id,
      'name': product.productName,
      'image': product.image,
      'quantity': quantity
    }).then((value) {});
  }
}
