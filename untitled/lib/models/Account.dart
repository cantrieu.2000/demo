import 'package:untitled/models/Users.dart';

class Account {
  Users? user;
  String? userName;
  String? avatar;
  String? phone;
  String? gender;
  String? date;
  String? id;

  Account(
      {this.user,
      this.userName,
      this.avatar,
      this.phone,
      this.gender,
      this.date,
      this.id});

  Account.fromJson(Map<String, dynamic> json)
      : user = json['user'],
        userName = json['userName'],
        avatar = json['avatar'],
        phone = json['phone'],
        gender = json['gender'],
        date = json['date'],
        id = json['id'];

  Map<String, dynamic> toJson() => {
        'user': user,
        'userName': userName,
        'avatar': avatar,
        'phone': phone,
        'gender': gender,
        'date': date,
        'id': id,
      };
}
