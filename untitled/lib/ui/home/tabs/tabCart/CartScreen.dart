import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled/ui/home/tabs/tabCart/CartController.dart';

class CartScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => BodyWidget();
}

class BodyWidget extends State<CartScreen> {
  final CartController _cartController = CartController();
  int num = 1;

  @override
  Widget build(BuildContext context) {
    _cartController.init();
    return GetBuilder<CartController>(
      init: _cartController,
      builder: (cartController) {
        return Scaffold(
            appBar: AppBar(
              title: Text('Cart Screen'),
              backgroundColor: Colors.deepOrange[400],
              actions: [
                IconButton(onPressed: () {}, icon: Icon(Icons.add)),
                IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.notifications_none_outlined)),
                IconButton(onPressed: () {}, icon: Icon(Icons.search))
              ],
            ),
            body: cartController.carts.length > 0
                ? Column(
                    children: [
                      Expanded(
                        child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            itemCount: cartController.carts.length,
                            itemBuilder: (context, index) {
                              return Stack(
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 2.0),
                                    padding: EdgeInsets.all(14),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            width: 1.0, color: Colors.grey)),
                                    height: MediaQuery.of(context).size.height *
                                        0.15,
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            child: Image.network(
                                              '${cartController.carts[index].image}',
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Column(
                                            children: [
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  '${cartController.carts[index].name}',
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Container(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  10)),
                                                      child: InkWell(
                                                        onTap: () {
                                                          cartController
                                                              .removeProduct(
                                                                  index);
                                                        },
                                                        child: Container(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    6),
                                                            color: Colors
                                                                    .deepOrange[
                                                                400],
                                                            child: Icon(
                                                                Icons.remove)),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Container(
                                                      child: Text(
                                                        "${(cartController.numbers[index])} Kg",
                                                        style: TextStyle(
                                                            fontSize: 18,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  10)),
                                                      child: InkWell(
                                                        onTap: () {
                                                          cartController
                                                              .addProduct(
                                                                  index);
                                                        },
                                                        child: Container(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    6),
                                                            color: Colors
                                                                    .deepOrange[
                                                                400],
                                                            child: Icon(
                                                                Icons.add)),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            child: Text('10 Discount'),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    child: IconButton(
                                      onPressed: () {
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              title: new Text(
                                                  "Bạn muốn xóa không ?"),
                                              actions: <Widget>[
                                                new FlatButton(
                                                  child: new Text("Không"),
                                                  onPressed: () {
                                                    Get.back();
                                                  },
                                                ),
                                                new FlatButton(
                                                  child: new Text("Có"),
                                                  onPressed: () {
                                                    cartController.delete(
                                                        '${cartController.carts[index].id}',
                                                        index);
                                                    Get.back();
                                                  },
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      },
                                      icon: Icon(
                                        Icons.clear,
                                        size: 28,
                                      ),
                                    ),
                                    top: 0,
                                    right: 0,
                                  )
                                ],
                              );
                            }),
                      ),
                      Card(
                        color: Colors.white70,
                        shadowColor: Colors.grey,
                        shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(25)),
                        ),
                        elevation: 20,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.vertical(
                                  top: Radius.circular(25)),
                              border:
                                  Border.all(width: 1.0, color: Colors.grey)),
                          height: 180,
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // Container(
                              //   margin: EdgeInsets.only(bottom: 14),
                              //   child: Row(
                              //     mainAxisAlignment: MainAxisAlignment.center,
                              //     children: [
                              //       ClipRRect(
                              //         borderRadius:
                              //             BorderRadius.all(Radius.circular(10)),
                              //         child: InkWell(
                              //           onTap: () {},
                              //           child: Container(
                              //               padding: EdgeInsets.all(6),
                              //               color: Colors.deepOrange[400],
                              //               child: Icon(Icons.remove)),
                              //         ),
                              //       ),
                              //       SizedBox(
                              //         width: 20,
                              //       ),
                              //       Container(
                              //         child: Text(
                              //           "Kg",
                              //           style: TextStyle(
                              //               fontSize: 18,
                              //               fontWeight: FontWeight.bold),
                              //         ),
                              //       ),
                              //       SizedBox(
                              //         width: 20,
                              //       ),
                              //       ClipRRect(
                              //         borderRadius:
                              //             BorderRadius.all(Radius.circular(10)),
                              //         child: InkWell(
                              //           onTap: () {},
                              //           child: Container(
                              //               padding: EdgeInsets.all(6),
                              //               color: Colors.deepOrange[400],
                              //               child: Icon(Icons.add)),
                              //         ),
                              //       )
                              //     ],
                              //   ),
                              // ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  color: Colors.deepOrange[400],
                                  child: Text("Add"),
                                  onPressed: () {},
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                : Center(
                    child: Text("Không có sản phẩm nào !"),
                  ));
      },
    );
  }
}
