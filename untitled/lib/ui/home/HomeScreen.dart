import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled/ui/home/HomeController.dart';
import 'package:untitled/ui/home/tabs/tabAccount/TabAccountScreen.dart';
import 'package:untitled/ui/home/tabs/tabHome/TabHomeScreen.dart';

class HomeScreen extends StatelessWidget {
  final dynamic user;

  HomeScreen({this.user});

  static const String title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title,
      home: BodyWidget(user),
    );
  }
}

class BodyWidget extends StatefulWidget {
  final dynamic user;

  BodyWidget(this.user);

  @override
  State<StatefulWidget> createState() => _BodyWidget();
}

class _BodyWidget extends State<BodyWidget> {
  final HomeController _homeController = HomeController();

  final _tabs = [TabHomeScreen(), TabAccountScreen()];

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        init: _homeController,
        builder: (homeController) {
          return Scaffold(
            body: Center(
              child: _tabs[homeController.selectedIndex.value],
            ),
            bottomNavigationBar: BottomNavigationBar(
              backgroundColor: Colors.deepOrangeAccent,
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: 'Home',
                  backgroundColor: Colors.red,
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.account_circle),
                  label: 'Account',
                  backgroundColor: Colors.green,
                ),
              ],
              currentIndex: homeController.selectedIndex.value,
              selectedItemColor: Colors.white,
              onTap: homeController.onItemTapped,
            ),
          );
        });
  }
}
