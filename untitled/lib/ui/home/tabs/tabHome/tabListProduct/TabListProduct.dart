import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled/models/Product.dart';
import 'package:untitled/ui/home/tabs/tabDetailProduct/DetailProductScreen.dart';

import 'TabListProductController.dart';

class TabListProductScreen extends StatefulWidget {
  final List<Product> list;

  TabListProductScreen(this.list);

  @override
  State<StatefulWidget> createState() => BodyWidget();
}

class BodyWidget extends State<TabListProductScreen> {
  final ListProductController _controller = ListProductController();
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _controller.getMoreList();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _controller.getProducts();
    return GetBuilder<ListProductController>(
        init: _controller,
        builder: (controller) {
          return Scaffold(
            appBar: AppBar(
              title: Text('Detail List Product'),
              backgroundColor: Colors.deepOrange[400],
              actions: [
                IconButton(onPressed: () {}, icon: Icon(Icons.add)),
                IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.notifications_none_outlined)),
                IconButton(onPressed: () {}, icon: Icon(Icons.search))
              ],
            ),
            body: ListView.builder(
                scrollDirection: Axis.vertical,
                controller: _scrollController,
                itemCount: controller.mList.length + 1,
                itemBuilder: (context, index) {
                  if (index == controller.mList.length) {
                    return controller.isCheck.value == false
                        ? Container(
                            height: 50,
                            child: CupertinoActivityIndicator(),
                          )
                        : Container();
                  }
                  return InkWell(
                    onTap: () {
                      Get.to(DetailProductScreen(widget.list[index]));
                    },
                    child: Container(
                      padding: EdgeInsets.all(14),
                      decoration: BoxDecoration(
                          border: Border.all(width: 1.0, color: Colors.grey)),
                      height: MediaQuery.of(context).size.height * 0.15,
                      child: Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Image.network(
                                '${controller.mList[index].image}',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            flex: 2,
                            child: Column(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    '${controller.mList[index].productName}',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                // Container(
                                //   child: Row(
                                //     mainAxisAlignment: MainAxisAlignment.start,
                                //     children: [
                                //       ClipRRect(
                                //         borderRadius: BorderRadius.all(
                                //             Radius.circular(10)),
                                //         child: InkWell(
                                //           onTap: () {
                                //             // cartController.removeProduct(index);
                                //           },
                                //           child: Container(
                                //               padding: EdgeInsets.all(6),
                                //               color: Colors.deepOrange[400],
                                //               child: Icon(Icons.remove)),
                                //         ),
                                //       ),
                                //       SizedBox(
                                //         width: 10,
                                //       ),
                                //       Container(
                                //         child: Text(
                                //           "1 Kg",
                                //           style: TextStyle(
                                //               fontSize: 18,
                                //               fontWeight: FontWeight.bold),
                                //         ),
                                //       ),
                                //       SizedBox(
                                //         width: 10,
                                //       ),
                                //       ClipRRect(
                                //         borderRadius: BorderRadius.all(
                                //             Radius.circular(10)),
                                //         child: InkWell(
                                //           onTap: () {
                                //             // cartController.addProduct(index);
                                //           },
                                //           child: Container(
                                //               padding: EdgeInsets.all(6),
                                //               color: Colors.deepOrange[400],
                                //               child: Icon(Icons.add)),
                                //         ),
                                //       )
                                //     ],
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                color: Colors.deepOrange[400],
                              ),
                              child: IconButton(
                                icon: Icon(Icons.add),
                                onPressed: () {
                                  showModel(controller, index);
                                },
                              )),
                        ],
                      ),
                    ),
                  );
                }),
          );
        });
  }

  void showModel(ListProductController controller, int index) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Card(
          margin: EdgeInsets.all(5),
          color: Colors.white70,
          shadowColor: Colors.grey,
          elevation: 20,
          child: Container(
            height: 200,
            decoration: BoxDecoration(
              border: Border.all(width: 1.0, color: Colors.grey),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 14),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Ink(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Colors.deepOrange[300],
                          ),
                          child: InkWell(
                            splashColor: Colors.deepOrange,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            onTap: () {
                              controller.removeProduct();
                            },
                            child: Container(
                                padding: EdgeInsets.all(6),
                                child: Icon(Icons.remove)),
                          )),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        child: Obx(() => Text(
                              "${controller.quantity.value} Kg",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            )),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Ink(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Colors.deepOrange[300],
                          ),
                          child: InkWell(
                            splashColor: Colors.deepOrange,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            onTap: () {
                              controller.addProduct();
                            },
                            child: Container(
                                padding: EdgeInsets.all(6),
                                child: Icon(Icons.add)),
                          )),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.6,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    color: Colors.deepOrange[400],
                    child: Text("Add"),
                    onPressed: () {
                      controller.addProductFirebase(
                          widget.list[index], controller.quantity.value);
                      Get.back();
                    },
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
