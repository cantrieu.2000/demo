import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:untitled/models/Product.dart';
import 'package:untitled/services/FirebaseApi.dart';

class TabHomeController extends GetxController {
  final firestoreInstance = FirebaseFirestore.instance;
  List<Product> products = [];
  List<Product> listDiscount = [];
  List<Product> listNewStock = [];
  List<Product> listNewBrands = [];
  List<Product> images = [];
  List<Product> listSearch = [];
  var query = "".obs;
  var isSearch = true.obs;

  init() async {
    products.clear();
    listDiscount.clear();
    listNewStock.clear();
    listNewBrands.clear();
    images.clear();
    getProducts();
    getNewBrands();
    getNewStocks();
    getImageSlider();
    update();
  }

  Future getProducts() async {
    try {
      await FirebaseApi.refProduct.get().then((data) {
        data.docs.forEach((result) {
          String id = result.id;
          String type = result["type"];
          String title = result["title"];
          String productName = result["productName"];
          String image = result["image"];
          String description = result["description"];
          String createAt = result["createAt"];
          Product product = Product(
              id, type, title, productName, image, description, createAt);
          products.add(product);
        });
      });

      if (products.isNotEmpty) {
        listDiscount =
            products.where((element) => element.type == "1").toList();
      }
    } catch (e) {}
    update();
  }

  Future getNewBrands() async {
    await FirebaseApi.refProduct
        .where('type', isEqualTo: '2')
        .get()
        .then((data) {
      data.docs.forEach((result) {
        String id = result.id;
        String type = result["type"];
        String title = result["title"];
        String productName = result["productName"];
        String image = result["image"];
        String description = result["description"];
        String createAt = result["createAt"];
        Product product =
            Product(id, type, title, productName, image, description, createAt);
        listNewBrands.add(product);
      });
    });
    update();
  }

  Future getNewStocks() async {
    await FirebaseApi.refProduct
        .where('type', isEqualTo: '3')
        .get()
        .then((data) {
      data.docs.forEach((result) {
        String id = result.id;
        String type = result["type"];
        String title = result["title"];
        String productName = result["productName"];
        String image = result["image"];
        String description = result["description"];
        String createAt = result["createAt"];
        Product product =
            Product(id, type, title, productName, image, description, createAt);
        listNewStock.add(product);
      });
    });
    update();
  }

  Future getImageSlider() async {
    var result = await FirebaseApi.refProduct
        .orderBy('createAt', descending: true)
        .limit(5)
        .get();
    result.docs.forEach((result) {
      String id = result.id;
      String type = result["type"];
      String title = result["title"];
      String productName = result["productName"];
      String image = result["image"];
      String description = result["description"];
      String createAt = result["createAt"];
      Product product =
          Product(id, type, title, productName, image, description, createAt);
      images.add(product);
    });
    update();
  }

  // Future getDataFireStore() async {
  //   await firestoreInstance.collection("user").get().then((data) {
  //     data.docs.forEach((result) {
  //       String id = result.id;
  //       String email = result["Email"];
  //       String userName = result["userName"];
  //       String url = result["url"];
  //       Account user =
  //           Account(email: email, userName: userName, url: url, id: id);
  //       users.add(user);
  //     });
  //   });
  //   update();
  // }

  Future<void> delete(String id) async {
    await firestoreInstance.collection("user").doc(id).delete().then((_) {
      print("success!");
    });
    update();
  }

  List<Product> performSearch() {
    listSearch.clear();
    for (int i = 0; i < products.length; i++) {
      Product item = products[i];

      if (item.title!.toLowerCase().contains(query.value.toLowerCase())) {
        listSearch.add(item);
      }
    }
    return listSearch;
  }

  void search() {
    if (query.value == "") {
      isSearch.value = true;
    } else {
      isSearch.value = false;
    }
    update();
  }
}
