import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart' as Path;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:untitled/services/FirebaseApi.dart';
import 'package:untitled/ui/home/HomeScreen.dart';

class TabAddController extends GetxController {
  late firebase_storage.Reference ref;

  // Future<void> addUser(
  //     String email, String userName, BuildContext context) async {
  //   await FirebaseApi.refUser.add({
  //     'Email': email, // John Doe
  //     'userName': userName, // Stokes and Sons// 42
  //   }).then((value) {
  //     print("add Success");
  //     Get.back();
  //   }).catchError((error) => print("Failed to add user: $error"));
  //   update();
  // }

  Future<void> updateProduct(
      String id,
      String type,
      String title,
      String productName,
      String description,
      BuildContext context) async {
    await FirebaseApi.firestoreInstance.collection("product").doc(id).update({
      "type": type,
      'title': title,
      'productName': productName,
      'description': description,
      'createAt': Timestamp.now().millisecondsSinceEpoch.toString(),
    }).then((data) {
      Get.off(HomeScreen());
      print("success!");
    });
    update();
  }

  final picker = ImagePicker();
  File? images;

  Future selectFile() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    images = File(pickedFile!.path);
    print("alo alo ${images!.path}");
    if (images == null) {
      retrieveLostData();
    }
    update();
  }

  Future<void> retrieveLostData() async {
    final LostData response = await picker.getLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      images = File(response.file!.path);
    } else {
      print(response.file);
    }
    update();
  }

  Future add(
      String type, String title, String productName, String description) async {
    if (images != null) {
      ref = firebase_storage.FirebaseStorage.instance
          .ref()
          .child('products/${Path.basename(images!.path)}');
      await ref.putFile(images!).whenComplete(() async {
        await ref.getDownloadURL().then((url) {
          FirebaseApi.refProduct.add({
            "type": type,
            'title': title,
            'productName': productName,
            'description': description,
            'image': url,
            'createAt': Timestamp.now().millisecondsSinceEpoch.toString(),
          }).then((value) {
            print("add Success");
            Get.off(HomeScreen());
          }).catchError((error) {
            print("Failed to add user: $error");
          });
        });
      });
    }
    update();
  }
}
