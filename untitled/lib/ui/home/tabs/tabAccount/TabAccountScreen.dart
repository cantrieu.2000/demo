import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled/contants/Contants.dart';
import 'package:untitled/res/colors.dart';
import 'package:untitled/ui/home/HomeScreen.dart';
import 'package:untitled/ui/home/tabs/tabAccount/TabAccountController.dart';
import 'package:untitled/ui/home/tabs/tabAdd/TabAddController.dart';

class TabAccountScreen extends StatelessWidget {
  final dynamic user;

  TabAccountScreen({this.user});

  static String _title = 'Account';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: BodyWidget(),
    );
  }
}

class BodyWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BodyWidget();
}

class _BodyWidget extends State<BodyWidget> {
  final TabAccountController _tabAccountController = TabAccountController();

  @override
  Widget build(BuildContext context) {
    // _tabAccountController.getAccount();
    return GetBuilder<TabAccountController>(
        init: _tabAccountController,
        builder: (accountController) {
          return Scaffold(
              appBar: AppBar(
                title: Text("Thông tin cá nhân"),
                centerTitle: true,
                backgroundColor: Colors.deepOrange[400],
              ),
              body: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                        height: 170,
                        width: MediaQuery.of(context).size.width,
                        child: Card(
                          child: Contants.account?.avatar == null
                              ? CupertinoActivityIndicator()
                              : Image.network(
                                  '${Contants.account?.avatar}',
                                  fit: BoxFit.cover,
                                  width: MediaQuery.of(context).size.width,
                                ),
                        )),
                    Divider(
                      height: 1,
                      color: Colors.black,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text('Email'),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              '${Contants.user?.email}',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal),
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.chevron_right,
                              size: 28,
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: Colors.black,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text('Mã uid'),
                          ),
                          Container(
                            width: 100,
                            alignment: Alignment.centerRight,
                            child: Text(
                              '${Contants.user?.uid}',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.chevron_right,
                              size: 28,
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: Colors.black,
                    ),
                    Container(
                      height: 15,
                      color: Colors.grey[400],
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text('Tên'),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              '${Contants.account?.userName}',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal),
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.chevron_right,
                              size: 28,
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: Colors.black,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text('Giới tính'),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              '${Contants.account?.gender}',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal),
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.chevron_right,
                              size: 28,
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: Colors.black,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text('Ngày sinh'),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              '${Contants.account?.date}',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal),
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.chevron_right,
                              size: 28,
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: Colors.black,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text('Điện thoại'),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              '${Contants.account?.phone}',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.chevron_right,
                              size: 28,
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: Colors.black,
                    ),
                    Container(
                      height: 15,
                      color: Colors.grey[400],
                    ),
                    MaterialButton(
                      onPressed: () {
                        Get.to(ChangePasswordScreen());
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text('Thay đổi mật khẩu'),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              '',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal),
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.chevron_right,
                              size: 28,
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: Colors.black,
                    ),
                    MaterialButton(
                      onPressed: () {
                        _tabAccountController.signOut();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text('Đăng xuất'),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              '',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal),
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.chevron_right,
                              size: 28,
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                    ),
                  ],
                ),
              ));
        });
  }
}

// ignore: must_be_immutable
class ChangePasswordScreen extends StatelessWidget {
  final TabAccountController _controller = Get.find();
  String _value = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Xác nhận mật khẩu"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(5),
            child: Text(
                "Để đảm bảo tài khoản của bạn luôn an toàn, vui lòng nhập mật khẩu của bạn để tiếp tục"),
          ),
          Container(
              padding: EdgeInsets.all(10),
              child: TextFormField(
                keyboardType: TextInputType.visiblePassword,
                obscureText: true,
                onChanged: (value) {
                  _value = value;
                  print(_value);
                },
                decoration: new InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: '',
                    labelText: "Mật khẩu mới",
                    labelStyle: TextStyle(color: primaryColor),
                    errorText: null),
                validator: null,
              )),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            width: MediaQuery.of(context).size.width,
            child: RaisedButton(
              onPressed: () {
                _controller.changePassword(_value, context);
              },
              child: Text("Tiếp tục"),
            ),
          )
        ],
      ),
    );
  }
}
