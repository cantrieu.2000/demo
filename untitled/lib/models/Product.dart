class Product {
  String? id;
  String? type;
  String? title;
  String? productName;
  String? image;
  String? description;
  String? createAt;

  Product(this.id, this.type, this.title, this.productName, this.image,
      this.description, this.createAt);

  Product.fromJson(Map<String, dynamic> json)
      : type = json['type'],
        title = json['title'],
        productName = json['productName'],
        image = json['image'],
        description = json['description'],
        createAt = json['createAt'];

  Map<String, dynamic> toJson() => {
        'type': type,
        'title': title,
        'productName': productName,
        'image': image,
        'description': description,
        'createAt': createAt,
      };
}
