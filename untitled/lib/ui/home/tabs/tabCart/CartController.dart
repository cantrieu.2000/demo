import 'package:get/get.dart';
import 'package:untitled/contants/Contants.dart';
import 'package:untitled/models/Cart.dart';
import 'package:untitled/services/FirebaseApi.dart';

class CartController extends GetxController {
  List<Cart> carts = [];
  var numbers = [].obs;

  init() {
    carts.clear();
    numbers.clear();
    getCartProducts();
  }

  void addProduct(int index) {
    if (numbers[index] < 1) {
      return;
    }
    numbers[index] = numbers[index] + 1;
    update();
  }

  void removeProduct(int index) {
    if (numbers[index] <= 1) {
      return;
    }
    numbers[index] = numbers[index] - 1;
    update();
  }

  Future delete(String id, int index) async {
    await FirebaseApi.refCartProduct
        .doc(Contants.account!.user?.uid)
        .collection(Contants.DETAIL_CART)
        .doc(id)
        .delete()
        .then((_) {
      carts.remove(carts[index]);
      print("ssss");
    });
    update();
  }

  Future getCartProducts() async {
    await FirebaseApi.refCartProduct
        .doc(Contants.account!.user?.uid)
        .collection(Contants.DETAIL_CART)
        .get()
        .then((data) {
      data.docs.forEach((result) {
        String id = result.id;
        String name = result["name"];
        String image = result["image"];
        int quantity = result["quantity"];
        Cart cart = Cart(id: id, name: name, image: image, quantity: quantity);
        carts.add(cart);
      });
    });
    for (int i = 0; i < carts.length; i++) {
      numbers.add(carts[i].quantity);
    }
    update();
  }
}
