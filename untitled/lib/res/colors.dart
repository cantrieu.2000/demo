import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const Color bgTitle = Color(0xFFFFF8E1);
const Color primaryColor = Color(0xff039145);
const Color white = Colors.white;
const Color errColor = Colors.red;