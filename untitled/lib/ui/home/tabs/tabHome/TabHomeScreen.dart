import 'dart:ui';

// ignore: import_of_legacy_library_into_null_safe
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:untitled/res/strings.dart';
import 'package:untitled/ui/home/tabs/tabAdd/TabAddProduct.dart';
import 'package:untitled/ui/home/tabs/tabCart/CartScreen.dart';
import 'package:untitled/ui/home/tabs/tabDetailProduct/DetailProductScreen.dart';
import 'package:untitled/ui/home/tabs/tabHome/HomeState.dart';
import 'package:untitled/ui/home/tabs/tabHome/tabListProduct/TabListProduct.dart';
import 'package:untitled/ui/home/tabs/tabHome/navDrawer.dart';

import 'TabHomeController.dart';

class TabHomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => BodyWidget();
}

class BodyWidget extends State<TabHomeScreen> {
  final TabHomeController _homeController = TabHomeController()..init();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TabHomeController>(
      init: _homeController,
      builder: (homeController) {
        return SafeArea(
          child: Scaffold(
            drawer: NavDrawer(),
            appBar: AppBar(
              backgroundColor: Colors.deepOrange[400],
              actions: [
                IconButton(
                    onPressed: () => Get.off(AddProduct()),
                    icon: Icon(Icons.add)),
                IconButton(
                    onPressed: () => Get.to(CartScreen()),
                    icon: Icon(Icons.add_shopping_cart)),
                IconButton(
                    onPressed: () => Get.off(AddProduct()),
                    icon: Icon(Icons.notifications_none_outlined)),
              ],
            ),
            body: SafeArea(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(8),
                        child: Text(
                          homeTitle,
                          style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ),
                      createSearchView(homeController, context),
                      homeController.isSearch.value
                          ? Column(
                        children: [
                          // ignore: unnecessary_null_comparison
                          homeController.images.isEmpty
                              ? CupertinoActivityIndicator()
                              : Container(
                              height: 200.0,
                              padding: EdgeInsets.all(8),
                              child: Carousel(
                                images: homeController.images.map((e) {
                                  return InkWell(
                                    onTap: () {
                                      Get.to(DetailProductScreen(e));
                                    },
                                    child: Image.network(
                                      e.image.toString(),
                                      fit: BoxFit.fill,
                                    ),
                                  );
                                }).toList(),
                                autoplay: true,
                                animationDuration:
                                Duration(milliseconds: 1000),
                                dotSize: 6.0,
                                dotSpacing: 15.0,
                                dotColor: Colors.lightGreenAccent,
                                borderRadius: true,
                              )),
                          Column(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.symmetric(
                                    vertical: 8, horizontal: 14),
                                child: Text("50% Discount",
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600)),
                              ),
                              Container(
                                height: 150,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount:
                                  homeController.listDiscount.length,
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      onTap: () {
                                        Get.to(DetailProductScreen(
                                            homeController
                                                .listDiscount[index]));
                                      },
                                      child: Card(
                                        margin: EdgeInsets.all(8),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(20))),
                                        child: ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(20),
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: <Widget>[
                                              Image.network(
                                                '${homeController.listDiscount[index].image}',
                                                fit: BoxFit.cover,
                                                height: 150,
                                                width: 250,
                                              ),
                                              Positioned.fill(
                                                child: Center(
                                                  child: BackdropFilter(
                                                    filter: ImageFilter.blur(
                                                      sigmaX: 0.0,
                                                      sigmaY: 0.0,
                                                    ),
                                                    child: Container(
                                                      color: Colors.black
                                                          .withOpacity(0.4),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                bottom: 20,
                                                child: Text(
                                                  '${homeController.listDiscount[index].title}',
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      color: Colors.white70,
                                                      fontWeight:
                                                      FontWeight.w600),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              )
                            ],
                          ),
                          Column(
                            children: [
                              MaterialButton(
                                onPressed: () {
                                  Get.to(TabListProductScreen(
                                      homeController.listNewStock));
                                },
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Text("New Stock",
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.black,
                                              fontWeight: FontWeight.w600)),
                                    ),
                                    Text(viewAll,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.amber,
                                            fontWeight: FontWeight.bold)),
                                    Icon(Icons.chevron_right)
                                  ],
                                ),
                              ),
                              Container(
                                height: 150,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount:
                                  homeController.listNewStock.length,
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      onTap: () {
                                        Get.to(DetailProductScreen(
                                            homeController
                                                .listNewStock[index]));
                                      },
                                      child: Card(
                                        margin: EdgeInsets.all(8),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(20))),
                                        child: ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(20),
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: <Widget>[
                                              Image.network(
                                                '${homeController.listNewStock[index].image}',
                                                fit: BoxFit.cover,
                                                height: 150,
                                                width: 250,
                                              ),
                                              Positioned.fill(
                                                child: Center(
                                                  child: BackdropFilter(
                                                    filter: ImageFilter.blur(
                                                      sigmaX: 0.0,
                                                      sigmaY: 0.0,
                                                    ),
                                                    child: Container(
                                                      color: Colors.black
                                                          .withOpacity(0.4),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                bottom: 20,
                                                child: Text(
                                                  '${homeController.listNewStock[index].title}',
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      color: Colors.white70,
                                                      fontWeight:
                                                      FontWeight.w600),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              )
                            ],
                          ),
                          Column(
                            children: [
                              MaterialButton(
                                onPressed: () {
                                  Get.to(TabListProductScreen(
                                      homeController.listNewBrands));
                                },
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Text("New Brands",
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.black,
                                              fontWeight: FontWeight.w600)),
                                    ),
                                    Text(viewAll,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.amber,
                                            fontWeight: FontWeight.bold)),
                                    Icon(Icons.chevron_right)
                                  ],
                                ),
                              ),
                              Container(
                                height: 150,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount:
                                  homeController.listNewBrands.length,
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      onTap: () {
                                        Get.to(DetailProductScreen(
                                            homeController
                                                .listNewBrands[index]));
                                      },
                                      child: Card(
                                        margin: EdgeInsets.all(8),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(20))),
                                        child: ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(20),
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: <Widget>[
                                              Image.network(
                                                '${homeController.listNewBrands[index].image}',
                                                fit: BoxFit.cover,
                                                height: 150,
                                                width: 250,
                                              ),
                                              Positioned.fill(
                                                child: Center(
                                                  child: BackdropFilter(
                                                    filter: ImageFilter.blur(
                                                      sigmaX: 0.0,
                                                      sigmaY: 0.0,
                                                    ),
                                                    child: Container(
                                                      color: Colors.black
                                                          .withOpacity(0.4),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                bottom: 20,
                                                child: Text(
                                                  '${homeController.listNewBrands[index].title}',
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      color: Colors.white70,
                                                      fontWeight:
                                                      FontWeight.w600),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              )
                            ],
                          ),
                        ],
                      )
                          : createSearchListView(homeController)
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
