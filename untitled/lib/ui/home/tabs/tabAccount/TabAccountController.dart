import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled/contants/Contants.dart';
import 'package:untitled/models/Account.dart';
import 'package:untitled/services/FirebaseApi.dart';
import 'package:untitled/ui/home/tabs/tabAccount/TabAccountScreen.dart';
import 'package:untitled/ui/login_signup/LoginSignUpScreen.dart';

class TabAccountController extends GetxController {
  Future signOut() async {
    await FirebaseApi.auth.signOut();
    Contants.account = null;
    Contants.user = null;
    Get.offAll(LoginSignUpScreen());
  }

  Future changePassword(String newPass, BuildContext context) async {
    final user = FirebaseApi.auth.currentUser!;
    user.updatePassword(newPass).then((_) {
      final snackBar =
          SnackBar(content: Text("Your password changed Succesfully"));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      // Get.off(TabAccountScreen());
    }).catchError((err) {
      print("You can't change the Password" + err.toString());
    });
  }

  Future getAccount() async {
    await FirebaseApi.refAccount
        .doc('${Contants.user?.uid}')
        .get()
        .then((data) {
      print(data.data());
      String userName = data['userName'];
      String phone = data['phone'];
      String gender = data['gender'];
      String avatar = data['avatar'];
      String date = data['date'];
      Contants.account = Account(
          user: Contants.user,
          userName: userName,
          avatar: avatar,
          phone: phone,
          gender: gender,
          date: date);
    });
    update();
  }
}
