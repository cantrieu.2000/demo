import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled/res/colors.dart';
import 'package:untitled/ui/home/HomeScreen.dart';
import 'package:untitled/ui/home/tabs/tabAdd/TabAddController.dart';

class AddProduct extends StatelessWidget {
  final String? type;
  final String? title;
  final String? productName;
  final String? image;
  final String? description;
  final String? id;

  AddProduct(
      {this.type,
      this.title,
      this.productName,
      this.image,
      this.description,
      this.id});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Product"),
        backgroundColor: Colors.deepOrange[400],
        leading: IconButton(
            onPressed: () {
              Get.to(HomeScreen());
            },
            icon: Icon(Icons.arrow_back)),
      ),
      body: BodyWidget(
        type: type,
        title: title,
        image: image,
        description: description,
        id: id,
      ),
    );
  }
}

class BodyWidget extends StatefulWidget {
  final String? type;
  final String? title;
  final String? productName;
  final String? image;
  final String? description;
  final String? id;

  BodyWidget(
      {this.type,
      this.title,
      this.productName,
      this.image,
      this.description,
      this.id});

  @override
  State<StatefulWidget> createState() => _BodyWidget();
}

class _BodyWidget extends State<BodyWidget> {
  final TabAddController _addController = TabAddController();
  TextEditingController typeController = new TextEditingController();
  TextEditingController titleController = new TextEditingController();
  TextEditingController productNameController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.title != null && widget.description != null) {
      typeController.text = widget.type!;
      titleController.text = widget.title!;
      productNameController.text = widget.productName!;
      descriptionController.text = widget.description!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TabAddController>(
      init: _addController,
      builder: (addController) {
        return Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    addController.selectFile();
                  },
                  child: widget.image != null
                      ? ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Image.network(
                            '${widget.image}',
                            height: 120,
                            width: 120,
                            fit: BoxFit.cover,
                          ),
                        )
                      : addController.images != null
                          ? ClipRRect(
                              child: Image.file(
                                addController.images!,
                                width: 100,
                                height: 100,
                                fit: BoxFit.fitHeight,
                              ),
                            )
                          : Text("Select image"),
                ),
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          width: MediaQuery.of(context).size.width,
                          child: Text("Type"),
                        ),
                        TextFormField(
                          controller: typeController,
                          keyboardType: TextInputType.text,
                          decoration: new InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "type",
                              labelStyle: TextStyle(color: primaryColor),
                              errorText: null),
                        )
                      ],
                    )),
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          width: MediaQuery.of(context).size.width,
                          child: Text("Title"),
                        ),
                        TextFormField(
                          controller: titleController,
                          keyboardType: TextInputType.text,
                          decoration: new InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "title",
                              labelStyle: TextStyle(color: primaryColor),
                              errorText: null),
                        )
                      ],
                    )),
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          width: MediaQuery.of(context).size.width,
                          child: Text("ProductName"),
                        ),
                        TextFormField(
                          controller: productNameController,
                          keyboardType: TextInputType.text,
                          decoration: new InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "productName",
                              labelStyle: TextStyle(color: primaryColor),
                              errorText: null),
                        )
                      ],
                    )),
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          width: MediaQuery.of(context).size.width,
                          child: Text("Description"),
                        ),
                        TextFormField(
                          controller: descriptionController,
                          keyboardType: TextInputType.text,
                          decoration: new InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "description",
                              labelStyle: TextStyle(color: primaryColor),
                              errorText: null),
                        )
                      ],
                    )),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        color: Colors.amber,
                        child: Text("Add"),
                        onPressed: () {
                          addController.add(
                              typeController.text,
                              titleController.text,
                              productNameController.text,
                              descriptionController.text);
                          typeController.clear();
                          titleController.clear();
                          productNameController.clear();
                          descriptionController.clear();
                          addController.images = null;
                        },
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        color: Colors.amber,
                        child: Text("update"),
                        onPressed: () {
                          addController.updateProduct(
                              widget.id!,
                              typeController.text,
                              titleController.text,
                              productNameController.text,
                              descriptionController.text,
                              context);
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
