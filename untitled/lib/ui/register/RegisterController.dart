import 'package:flutter/cupertino.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/get.dart';

class RegisterController extends GetxController {
  var textErr = "".obs;
  var hoTen = "".obs;
  var errHoTen = "".obs;
  var quocTich = "".obs;
  var errQuocTich = "".obs;
  var gioiTinh = "".obs;
  var errGioiTinh = "".obs;
  var soNha = "".obs;
  var errsoNha = "".obs;
  var xaPhuong = "".obs;
  var errxaPhuong = "".obs;
  var quanHuyen = "".obs;
  var errquanHuyen = "".obs;
  var tinh = "".obs;
  var errTinh = "".obs;
  var cmt = "".obs;
  var errCmt = "".obs;
  var ngheNghiep = "".obs;
  var errNgheNghiep = "".obs;
  var email = "".obs;
  var errEmail = "".obs;
  var ngaySinh = "".obs;
  var errNgaySinh = "".obs;
  var soTruong = "".obs;
  var errSoTruong = "".obs;
  var trinhDoVH = "".obs;
  var errtrinhDoVH = "".obs;
  var code = "".obs;
  var errcode = "".obs;
  var phone = "".obs;
  var errphone = "".obs;
  var userName = "".obs;
  var errUserName = "".obs;

  void validator() {
    validatorHoTen();
    validatorQuocTich();
    validatorGioiTinh();
    validatorUserName();
    validatorPhone();
    validatorSoTruong();
    validatorTrinhDoVH();
    validatorCMT();
    validatorCode();
    validatorTinh();
    validatorQuanHuyen();
    validatorSoNha();
    validatorEmail();
    validatorNgheNghiep();
    validatorNgaySinh();
  }

  void validatorHoTen() {
    if (hoTen.value == "") {
      errHoTen.value = "Hãy nhập ho ten";
    } else {
      errHoTen.value = "";
    }
    update();
  }

  void validatorQuocTich() {
    if (quocTich.value == "") {
      errQuocTich.value = "Hãy nhập giá trị";
    } else {
      errQuocTich.value = "";
    }
    update();
  }

  void validatorGioiTinh() {
    if (gioiTinh.value == "") {
      errGioiTinh.value = "Hãy nhập giá trị";
    } else {
      errGioiTinh.value = "";
    }
    update();
  }

  validatorSoNha() {
    if (soNha.value == "") {
      errsoNha.value = "Hãy nhập giá trị";
    } else {
      errsoNha.value= "";
    }
    update();
  }

  validatorXaPhuong() {
    if (xaPhuong.value == "") {
      errxaPhuong.value = "Hãy nhập giá trị";
    } else {
      errxaPhuong.value = "";
    }
    update();
  }

  validatorQuanHuyen() {
    if (quanHuyen.value == "") {
      errquanHuyen.value = "Hãy nhập giá trị";
    } else {
      errquanHuyen.value = "";
    }
    update();
  }

  validatorTinh() {
    if (tinh.value == "") {
      errTinh.value = "Hãy nhập giá trị";
    } else {
      errTinh.value = "";
    }
    update();
  }

  validatorCMT() {
    if (cmt.value == "") {
      errCmt.value = "Hãy nhập giá trị";
    } else {
      errCmt.value = "";
    }
    update();
  }

  validatorNgheNghiep() {
    if (ngheNghiep.value == "") {
      errNgheNghiep.value = "Hãy nhập giá trị";
    } else {
      errNgheNghiep.value = "";
    }
    update();
  }

  validatorEmail() {
    if (email.value == "") {
      errEmail.value = "Hãy nhập giá trị";
    } else {
      errEmail.value = "";
    }
    update();
  }

  validatorNgaySinh() {
    if (email.value == "") {
      errNgaySinh.value = "Hãy nhập giá trị";
    } else {
      errNgaySinh.value = "";
    }
    update();
  }

  validatorSoTruong() {
    if (soTruong.value == "") {
      errSoTruong.value = "Hãy nhập giá trị";
    } else {
      errSoTruong.value = "";
    }
    update();
  }

  validatorTrinhDoVH() {
    if (trinhDoVH.value == "") {
      errtrinhDoVH.value = "Hãy nhập giá trị";
    } else {
      errtrinhDoVH.value = "";
    }
    update();
  }

  validatorCode() {
    if (code.value == "") {
      errcode.value = "Hãy nhập giá trị";
    } else {
      errcode.value = "";
    }
    update();
  }

  validatorPhone() {
    if (phone.value == "") {
      errphone.value = "Hãy nhập giá trị";
    } else {
      errphone.value = "";
    }
    update();
  }

  validatorUserName() {
    if (userName.value == "") {
      errUserName.value = "Hãy nhập giá trị";
    } else {
      errUserName.value = "";
    }
    update();
  }
}
