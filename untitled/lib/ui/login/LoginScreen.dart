import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled/res/colors.dart';
import 'package:untitled/ui/home/HomeScreen.dart';
import 'package:untitled/ui/login/LoginController.dart';
import 'package:untitled/ui/login/LoginDelegate.dart';
import 'package:untitled/view/showNotification.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: BodyWidget(),
    );
  }
}

class BodyWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BodyWidget();
}

class _BodyWidget extends State<BodyWidget> implements LoginDelegate {
  final LoginController _loginController = LoginController();
  TextEditingController userController = new TextEditingController();
  TextEditingController passController = new TextEditingController();

  bool _rePasswordVisible = true;
  bool rememberMe = true;

  @override
  void initState() {
    super.initState();
    userController.addListener(() {
      _loginController.listenUserPass(userController.text, passController.text);
    });
    passController.addListener(() {
      _loginController.listenUserPass(userController.text, passController.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    _loginController.init(userController.text, passController.text, context);
    _loginController.setDelegate(this);
    return GetBuilder<LoginController>(
      init: _loginController,
      builder: (loginController) {
        return Scaffold(
          body: Stack(
            children: [
              Positioned(
                  child: SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: Container(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset("images/ic_matrix.png"),
                      SizedBox(
                        height: 20,
                      ),
                      _buildTextInputUserName((value) {
                        loginController.validateUser(value);
                      }, loginController.errUser.value, "Email"),
                      _buildTextInputPass((value) {
                        loginController.validatePass(value);
                      }, loginController.errPass.value, "Mật khẩu"),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Checkbox(
                              value: rememberMe,
                              onChanged: (value) {
                                setState(() {
                                  rememberMe = !rememberMe;
                                });
                              }),
                          Container(
                            child: Text("Nhớ mật khẩu"),
                          )
                        ],
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.6,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          color: Colors.amber,
                          child: Text("Đăng nhập"),
                          onPressed: () {
                            loginController.login(userController.text,
                                passController.text, context);
                          },
                        ),
                      )
                    ],
                  ),
                ),
              )),
              Obx(
                () => loginController.showLoading.value == false
                    ? Container()
                    : Center(child: CircularProgressIndicator()),
              )
            ],
          ),
        );
      },
    );
  }

  Widget _buildTextInputPass(
      Function(String) getValue, String err, String title) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        alignment: Alignment.centerLeft,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              controller: passController,
              obscureText: _rePasswordVisible,
              keyboardType: TextInputType.visiblePassword,
              onChanged: getValue,
              decoration: new InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: '',
                  suffixIcon: IconButton(
                    icon: Icon(
                      _rePasswordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: primaryColor,
                    ),
                    onPressed: () {
                      setState(() {
                        _rePasswordVisible = !_rePasswordVisible;
                      });
                    },
                  ),
                  labelText: title,
                  labelStyle: TextStyle(color: primaryColor),
                  errorText: err == "" ? null : err),
              validator: null,
            )
          ],
        ));
  }

  Widget _buildTextInputUserName(
      Function(String) getValue, String err, String title) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        alignment: Alignment.centerLeft,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Text(title),
            ),
            TextFormField(
              controller: userController,
              keyboardType: TextInputType.emailAddress,
              onChanged: getValue,
              decoration: new InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: '',
                  labelText: title,
                  labelStyle: TextStyle(color: primaryColor),
                  errorText: err == "" ? null : err),
              validator: null,
            )
          ],
        ));
  }

  @override
  void showLoading(String message) {
    ShowNotification.showToast(context, message, errColor);
  }

  @override
  void goToHomeScreen() {
    Get.off(HomeScreen());
  }
}
