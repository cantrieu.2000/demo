import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class FirebaseApi {
  static FirebaseAuth auth = FirebaseAuth.instance;
  static final firestoreInstance = FirebaseFirestore.instance;
  static CollectionReference refAccount =
      FirebaseFirestore.instance.collection('account');
  static CollectionReference refProduct =
      FirebaseFirestore.instance.collection('product');
  static CollectionReference refCartProduct =
      FirebaseFirestore.instance.collection('cart');
}
