class Cart {
  String? id;
  String? name;
  String? image;
  int? quantity;

  Cart({this.id, this.name, this.image, this.quantity});
}
