import 'package:firebase_auth/firebase_auth.dart';

abstract class LoginDelegate {
  void goToHomeScreen();

  void showLoading(String message);
}
