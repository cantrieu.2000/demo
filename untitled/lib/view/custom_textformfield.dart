import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/res/colors.dart';

class CustomTextFormField extends StatelessWidget {
  final TextEditingController? controller;
  final FormFieldValidator<String>? validator;
  final int? maxLine;
  final Key? formKey;
  final String? hintText;
  final TextStyle? style;
  final Function(String)? onChange;
  final TextInputAction? textInputAction;
  final TextInputType? textInputType;
  final double? contentPaddingHorizontal;
  final double? contentPaddingVertical;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final bool? readOnly;
  final FocusNode? focusNode;
  final EdgeInsetsGeometry? contentPadding;
  final TextCapitalization? textCapitalization;

  CustomTextFormField(
      {this.controller,
      this.validator,
      this.maxLine,
      this.formKey,
      this.hintText,
      this.style,
      this.onChange,
      this.textInputAction,
      this.textInputType,
      this.contentPaddingHorizontal,
      this.contentPaddingVertical,
      this.suffixIcon,
      this.prefixIcon,
      this.readOnly = false,
      this.focusNode,
      this.contentPadding,
      this.textCapitalization});

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(splashColor: Colors.white),
      child: TextFormField(
        onChanged: onChange,
        readOnly: readOnly!,
        controller: controller,
        validator: validator,
        focusNode: focusNode,
        style: style,
        keyboardType: textInputType,
        textInputAction: textInputAction ?? TextInputAction.newline,
        decoration: InputDecoration(
          hintText: hintText,
          filled: true,
          suffixIcon: suffixIcon,
          prefixIcon: prefixIcon,
          fillColor: Colors.white,
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: primaryColor, width: 1.0),
            borderRadius: const BorderRadius.all(Radius.circular(8)),
          ),
          contentPadding: contentPadding ??
              EdgeInsets.symmetric(
                  vertical: contentPaddingVertical ?? 17.0,
                  horizontal: contentPaddingHorizontal ?? 16.0),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 1.0),
            borderRadius: const BorderRadius.all(Radius.circular(8)),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 0.5),
            borderRadius: const BorderRadius.all(Radius.circular(8)),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.red),
            borderRadius: const BorderRadius.all(Radius.circular(8)),
          ),
        ),
        textCapitalization: textCapitalization ?? TextCapitalization.none,
        maxLines: maxLine,
      ),
    );
  }
}
