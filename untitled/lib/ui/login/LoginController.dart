import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled/contants/Contants.dart';
import 'package:untitled/models/Account.dart';
import 'package:untitled/models/Users.dart';
import 'package:untitled/services/FirebaseApi.dart';
import 'package:untitled/ui/login/LoginDelegate.dart';

class LoginController extends GetxController {
  var errPass = "".obs;
  var errUser = "".obs;
  late LoginDelegate _loginDelegate;

  setDelegate(LoginDelegate loginDelegate) {
    _loginDelegate = loginDelegate;
  }

  bool btShowLogin = false;
  var showLoading = false.obs;

  init(String email, String pass, BuildContext context) {
    // FirebaseApi.auth.authStateChanges().listen((user) {
    //   if (user != null) {
    //     showLoading.value = false;
    //     Contants.account = Account(email: user.email, uid: user.uid);
    //     _loginDelegate.goToHomeScreen();
    //   } else {
    //     login(email, pass, context);
    //   }
    // });
  }

  Future login(String userName, String pass, BuildContext context) async {
    bool check = false;
    showLoading.value = true;
    if (!validateUser(userName)) {
      errUser.value = "Sai định dạng email";
      check = true;
    }
    if (!validatePass(pass)) {
      errPass.value = "Mật khẩu tối thiểu 6 kí tự";
      check = true;
    }
    if (check == false) {
      try {
        UserCredential authResult = await FirebaseApi.auth
            .signInWithEmailAndPassword(email: userName, password: pass);
        if (authResult.user != null) {
          showLoading.value = false;
          Contants.user =
              Users(email: authResult.user?.email, uid: authResult.user?.uid);
          _loginDelegate.goToHomeScreen();
        } else {
          showLoading.value = false;
          _loginDelegate.showLoading("Đăng nhập thất bại");
        }
      } catch (e) {}
    }
    showLoading.value = false;
    update();
  }

  Future register() async {
    try {
      UserCredential authResult = await FirebaseApi.auth
          .createUserWithEmailAndPassword(
              email: 'trieu@gmail.com', password: '123456');
      // _loginDelegate.goToHomeScreen();
    } catch (e) {}
  }

  void listenUserPass(String user, String pass) {
    btShowLogin = true;
    if (!validateUser(user)) {
      if (user != "") {
        errUser.value = "";
      } else {
        errUser.value = "Bạn không được để trống";
      }
      btShowLogin = false;
    } else {
      errUser.value = "";
    }
    if (!validatePass(pass)) {
      if (pass.length > 0) {
        errPass.value = "";
      } else {
        errPass.value = "Bạn không được để trống";
      }
      btShowLogin = false;
    } else {
      errPass.value = "";
    }
    update();
  }

  bool validatePass(String pass) {
    if (pass.length < 6) {
      return false;
    } else {
      return true;
    }
  }

  bool validateUser(String userName) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(userName)) {
      return false;
    }
    return true;
  }
}
