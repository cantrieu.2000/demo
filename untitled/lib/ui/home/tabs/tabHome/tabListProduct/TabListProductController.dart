import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:untitled/contants/Contants.dart';
import 'package:untitled/models/Product.dart';
import 'package:untitled/services/FirebaseApi.dart';

class ListProductController extends GetxController {
  List<Product> mList = [];
  Product? lastProduct;
  var count = 10.obs;
  var isCheck = false.obs;
  var perPage = 10;

  init(List<Product> list) {
    if (count.value < list.length) {
      for (int i = 0; i < count.value; i++) {
        mList.add(list[i]);
      }
    } else {
      mList = list;
    }
  }

  // Future getMoreList(List<Product> list) async {
  //   await Future.delayed(Duration(seconds: 0, milliseconds: 2000))
  //       .then((value) {});
  //   int num = list.length - count.value;
  //   if (num == 0) {
  //     isCheck.value = false;
  //   }
  //   if (num > count.value && (num - count.value) >= 5) {
  //     for (int i = count.value; i < count.value + 5; i++) {
  //       mList.add(list[i]);
  //     }
  //     count.value = count.value + 5;
  //     isCheck.value = true;
  //   }
  //   if (num < count.value) {
  //     for (int a = count.value; a < count.value + num; a++) {
  //       mList.add(list[a]);
  //     }
  //     count.value += num;
  //     isCheck.value = true;
  //   }
  //   update();
  // }

  Future getProducts() async {
    await FirebaseApi.refProduct
        .orderBy('createAt', descending: true)
        .limit(perPage)
        .get()
        .then((data) {
      data.docs.forEach((result) {
        mList.add(addItemProduct(result));
      });
    });
    lastProduct = mList[mList.length - 1];
    update();
  }

  Future getMoreList() async {
    await Future.delayed(Duration(seconds: 0, milliseconds: 1000))
        .then((value) {});
    var data = await FirebaseApi.refProduct
        .orderBy('createAt', descending: true)
        .startAfter([lastProduct!.createAt])
        .limit(perPage)
        .get();
    if (data.docs.length < perPage) {
      data.docs.forEach((result) {
        mList.add(addItemProduct(result));
        lastProduct = mList[mList.length - 1];
        isCheck.value = true;
      });
    } else if (data.docs.length == perPage) {
      data.docs.forEach((result) {
        mList.add(addItemProduct(result));
        lastProduct = mList[mList.length - 1];
        isCheck.value = false;
      });
    }
    update();
  }

  addItemProduct(var result) {
    String id = result.id;
    String type = result["type"];
    String title = result["title"];
    String productName = result["productName"];
    String image = result["image"];
    String description = result["description"];
    String createAt = result["createAt"];

    return Product(id, type, title, productName, image, description, createAt);
  }

  Future addProductFirebase(Product product, int quantity) async {
    FirebaseApi.refCartProduct
        .doc(Contants.account!.user?.uid)
        .collection(Contants.DETAIL_CART)
        .add({
      'id': product.id,
      'name': product.productName,
      'image': product.image,
      'quantity': quantity
    }).then((value) {});
  }

  var quantity = 1.obs;

  void addProduct() {
    if (quantity.value < 1) {
      return;
    }
    quantity.value = quantity.value + 1;
    update();
  }

  void removeProduct() {
    if (quantity.value <= 1) {
      return;
    }
    quantity.value = quantity.value - 1;
    update();
  }
}
