import 'package:flutter/material.dart';
import 'package:untitled/contants/Contants.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Material(
        color: Colors.deepPurple,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircleAvatar(
                      radius: 30,
                      backgroundImage:
                          NetworkImage('${Contants.account?.avatar}')),
                  SizedBox(width: 20),
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${Contants.account?.userName}',
                        style: TextStyle(fontSize: 20.0, color: Colors.white),
                        overflow: TextOverflow.ellipsis,
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        '${Contants.user?.email}',
                        style: TextStyle(fontSize: 14.0, color: Colors.white),
                      ),
                    ],
                  )),
                  CircleAvatar(
                    radius: 24,
                    backgroundColor: Colors.black12,
                    child: IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.add_comment_outlined),
                    ),
                  )
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1.0, color: Color(0xFF7F7F7F)),
                ),
              ),
              child: ListTile(
                leading: Icon(Icons.input, color: Colors.white),
                title: Text('Welcome', style: TextStyle(color: Colors.white)),
                onTap: () => {},
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1.0, color: Color(0xFF7F7F7F)),
                ),
              ),
              child: ListTile(
                leading: Icon(Icons.verified_user, color: Colors.white),
                title: Text('Profile', style: TextStyle(color: Colors.white)),
                onTap: () => {},
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1.0, color: Color(0xFF7F7F7F)),
                ),
              ),
              child: ListTile(
                leading: Icon(Icons.settings, color: Colors.white),
                title: Text('Settings', style: TextStyle(color: Colors.white)),
                onTap: () => {},
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1.0, color: Color(0xFF7F7F7F)),
                ),
              ),
              child: ListTile(
                leading: Icon(Icons.border_color, color: Colors.white),
                title: Text('Feedback', style: TextStyle(color: Colors.white)),
                onTap: () => {},
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1.0, color: Color(0xFF7F7F7F)),
                ),
              ),
              child: ListTile(
                leading: Icon(Icons.exit_to_app, color: Colors.white),
                title: Text('Logout', style: TextStyle(color: Colors.white)),
                onTap: () => {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}
