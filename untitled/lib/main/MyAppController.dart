import 'dart:async';

import 'package:get/get.dart';
import 'package:untitled/contants/Contants.dart';
import 'package:untitled/models/Account.dart';
import 'package:untitled/models/Users.dart';
import 'package:untitled/services/FirebaseApi.dart';
import 'package:untitled/ui/home/HomeScreen.dart';
import 'package:untitled/ui/login_signup/LoginSignUpScreen.dart';

class MyAppController extends GetxController {
  final oneSec = const Duration(seconds: 2);

  init() {
    Timer(oneSec, () {
      checkLogin();
    });
  }

  Future checkLogin() async {
    FirebaseApi.auth.authStateChanges().listen((user) async {
      if (user != null) {
        Contants.user = Users(email: user.email, uid: user.uid);
        await FirebaseApi.refAccount.doc('${user.uid}').get().then((data) {
          String userName = data['userName'];
          String phone = data['phone'];
          String gender = data['gender'];
          String avatar = data['avatar'];
          String date = data['date'];
          Contants.account = Account(
              user: Contants.user,
              userName: userName,
              avatar: avatar,
              phone: phone,
              gender: gender,
              date: date);
        });
        Get.off(HomeScreen());
      } else {
        Get.off(LoginSignUpScreen());
      }
    });
    update();
  }

  Future getAccount() async {
    await FirebaseApi.refAccount
        .doc('${Contants.user?.uid}')
        .get()
        .then((data) {
      print(data.data());
      String userName = data['userName'];
      String phone = data['phone'];
      String gender = data['gender'];
      String avatar = data['avatar'];
      String date = data['date'];
      Contants.account = Account(
          user: Contants.user,
          userName: userName,
          avatar: avatar,
          phone: phone,
          gender: gender,
          date: date);
    });
    update();
  }
}
