import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled/models/Product.dart';
import 'package:untitled/ui/home/tabs/tabCart/CartScreen.dart';
import 'package:untitled/ui/home/tabs/tabDetailProduct/DetailProductController.dart';

class DetailProductScreen extends StatefulWidget {
  final Product product;

  DetailProductScreen(this.product);

  @override
  State<StatefulWidget> createState() => BodyWidget();
}

class BodyWidget extends State<DetailProductScreen> {
  final DetailProductController _detailProductController =
      DetailProductController();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DetailProductController>(
        init: _detailProductController,
        builder: (detailController) {
          return Scaffold(
            appBar: AppBar(
              title: Text('${widget.product.title}'),
              backgroundColor: Colors.deepOrange[400],
              actions: [
                IconButton(onPressed: () {}, icon: Icon(Icons.add)),
                IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.notifications_none_outlined)),
                IconButton(onPressed: () {}, icon: Icon(Icons.search))
              ],
            ),
            body: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(8),
                  child: Image.network(
                    '${widget.product.image}',
                    fit: BoxFit.fill,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.3,
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 12),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    '${widget.product.productName}',
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.3,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  alignment: Alignment.topLeft,
                  child: SingleChildScrollView(
                    child: Text(
                      '${widget.product.description}',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
                Expanded(
                  child: Card(
                    margin: EdgeInsets.all(5),
                    color: Colors.white70,
                    shadowColor: Colors.grey,
                    elevation: 20,
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: 1.0, color: Colors.grey),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(bottom: 14),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  child: InkWell(
                                    onTap: () {
                                      detailController.removeProduct();
                                    },
                                    child: Container(
                                        padding: EdgeInsets.all(6),
                                        color: Colors.deepOrange[400],
                                        child: Icon(Icons.remove)),
                                  ),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Container(
                                  child: Obx(() => Text(
                                        "${detailController.count} Kg",
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      )),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  child: InkWell(
                                    onTap: () {
                                      detailController.addProduct();
                                    },
                                    child: Container(
                                        padding: EdgeInsets.all(6),
                                        color: Colors.deepOrange[400],
                                        child: Icon(Icons.add)),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                              color: Colors.deepOrange[400],
                              child: Text("Add"),
                              onPressed: () {
                                detailController.addProductFirebase(
                                    widget.product,
                                    detailController.count.value);
                                Get.back();
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
