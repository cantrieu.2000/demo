import 'package:flutter/material.dart';
import 'package:untitled/res/colors.dart';

class ShowNotification{
  static void showToast(BuildContext context, String title, Color color) async {
    if(context == null){return;}
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        backgroundColor: color,
        content: Text(title),
        action: SnackBarAction(
            label: 'x',
            textColor: white, onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
    await Future.delayed(Duration(seconds: 2),scaffold.hideCurrentSnackBar);
  }
}